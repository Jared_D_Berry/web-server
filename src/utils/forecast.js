const request = require('request')

const forecast = (longitude, latitude, callback) => {
    const url = 'http://api.weatherstack.com/current?access_key=b2f2ec86d9b7c7a58151e596125660b0&query=' + latitude + ',' + longitude + '&units=m'

    request({url, json: true}, (error, { body }) => {
        if (error) {
            callback('Unable to connect to weather service!', undefined)
        } else if (body.error) {
            callback('Unable to find location', undefined)
        } else {
            callback(undefined, body.current.weather_descriptions[0] + ' with a ' + body.current.humidity + ' % humidity. Temperature is ' + body.current.temperature + ' degrees out but feels like ' + body.current.feelslike + ' degrees out with wind speeds of ' + body.current.wind_speed + ' and at a direction of ' + body.current.wind_degree + ' degree.')
        }
    })
}

module.exports = forecast